package com.techuniversity.calculadora;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
//import io.corp.calculator.TracerImpl;
import java.math.BigDecimal;
import java.util.List;

@RestController()
@RequestMapping(path = "/apicalc/v1")
public class CalculadoraController {

    @Autowired
    private ICalculadoraService calculadoraService;

    //@Autowired
    //CalculadoraService calculadoraService;

    @GetMapping(value = "/calcula")
    public ResponseEntity<Double> calcula(@RequestParam(name = "total depositado") BigDecimal totalDepositado,
                                          @RequestParam(name = "total rendimiento") BigDecimal totalRendimiento,
                                          @RequestParam(name = "total monto") BigDecimal totalMonto,
                                          @RequestParam(name = "operacion") String operacion) {

        double result = this.calculadoraService.calcula(totalDepositado, totalRendimiento, totalMonto, operacion);
        //tracer.trace(result);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
