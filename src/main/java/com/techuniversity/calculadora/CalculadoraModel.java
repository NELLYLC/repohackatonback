package com.techuniversity.calculadora;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Document(collection = "productos")
public class CalculadoraModel {
    @Id
    @NotNull
    private Double deposito;
    private Double aportacion;
    private String frecuencia;
    private Double tasa;
    private Integer periodo;
    private Double totaldepositado;
    private Double totalrendimiento;
    private  Double montototal;

    public CalculadoraModel() {
    }

    public CalculadoraModel(@NotNull Double deposito, Double aportacion, String frecuencia, Double tasa, Integer periodo, Double totaldepositado, Double totalrendimiento, Double montototal) {
        this.deposito = deposito;
        this.aportacion = aportacion;
        this.frecuencia = frecuencia;
        this.tasa = tasa;
        this.periodo = periodo;
        this.totaldepositado = totaldepositado;
        this.totalrendimiento = totalrendimiento;
        this.montototal = montototal;
    }

    public Double getDeposito() {
        return deposito;
    }

    public void setDeposito(Double deposito) {
        this.deposito = deposito;
    }

    public Double getAportacion() {
        return aportacion;
    }

    public void setAportacion(Double aportacion) {
        this.aportacion = aportacion;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public Double getTasa() {
        return tasa;
    }

    public void setTasa(Double tasa) {
        this.tasa = tasa;
    }

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public Double getTotaldepositado() {
        return totaldepositado;
    }

    public void setTotaldepositado(Double totaldepositado) {
        this.totaldepositado = totaldepositado;
    }

    public Double getTotalrendimiento() {
        return totalrendimiento;
    }

    public void setTotalrendimiento(Double totalrendimiento) {
        this.totalrendimiento = totalrendimiento;
    }

    public Double getMontototal() {
        return montototal;
    }

    public void setMontototal(Double montototal) {
        this.montototal = montototal;
    }
}
