package com.techuniversity.calculadora;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculadoraRepository extends MongoRepository<CalculadoraModel, String> {

}
