package com.techuniversity.calculadora;
//import com.iteram.utilidades.Operacion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class CalculadoraService implements ICalculadoraService{
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculadoraService.class);

    @Override
    public double calcula(BigDecimal totalDepositado, BigDecimal totalRendimiento, BigDecimal totalMonto, String opTexto) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Calculando resultado para : {} {} {}", totalDepositado, totalRendimiento, totalMonto, opTexto);
        }

        Operacion operacion = Operacion.desdeValor(opTexto);

        if(operacion == null) {
            throw new RuntimeException("Operación imposible de procesar: " + opTexto);
        }

        switch (operacion) {
            case SUMA:
                return totalDepositado.add(totalRendimiento).doubleValue();
           default:
                if(LOGGER.isErrorEnabled()) {
                    LOGGER.error("Operación no soportada para ser calculada: {}", operacion);
                }
                throw new RuntimeException("Operación no soportada para ser calculada: " + operacion.toString());

        }
    }
}
