package com.techuniversity.calculadora;

import java.math.BigDecimal;

public interface ICalculadoraService {
    double calcula(BigDecimal totalDepositado, BigDecimal totalRendimiento, BigDecimal totalMonto, String operacion);
}
