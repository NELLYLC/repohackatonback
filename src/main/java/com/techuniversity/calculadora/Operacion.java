package com.techuniversity.calculadora;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum Operacion {
    SUMA("+"),
    RESTA("-"),
    MULTIPLICACION("*"),
    DIVISION("/");

    private static final Operacion[] valores = new Operacion[]{SUMA, RESTA, MULTIPLICACION, DIVISION};

    private String signo;

    Operacion(String signo) {
        this.signo = signo;
    }

    private String getSigno() {
        return this.signo;
    }

    @JsonCreator
    public static Operacion desdeValor(String valor) {

        for (int i = 0; i < valores.length; ++i) {
            Operacion opActual = valores[i];
            if (valor.equalsIgnoreCase(opActual.name()) ||
                    valor.equalsIgnoreCase(opActual.getSigno())) {
                return opActual;
            }
        }

        throw new RuntimeException("Operación no soportada para el valor: " + valor);
    }
}

}
