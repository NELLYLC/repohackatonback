package com.techuniversity.clientes;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.http.HttpStatus;
import java.util.List;
import java.util.Map;

@RestController

@RequestMapping("/apicli/v1")

public class ClienteController {
    //@Autowired
    //public BCryptPasswordEncoder bCryptEncoder;

    //@PostMapping (path = "/bcrypt")
    //public ResponseEntity<Object> bcrypt(@RequestBody Map<String, String> pass){
       // String passwordEncode = bCryptEncoder.encode(pass.get("password").toString());
       // return ResponseEntity.status(HttpStatus.OK).body(passwordEncode);
    //    return ResponseEntity.ok("OK");
    //}


    @GetMapping("/clientes")
    public List getClientes(@RequestBody String filtro){
        return ClienteService.getFiltrados(filtro);
    }

    @PostMapping("/clientes")
    public String setClientes(@RequestBody String newCliente){
        try {
            ClienteService.insert(newCliente);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }
    @PutMapping("/clientes")
    public String updClientes(@RequestBody String data){
        try {
            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String updates = obj.getJSONObject("updates").toString();
            ClienteService.update(filtro, updates);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }


}
