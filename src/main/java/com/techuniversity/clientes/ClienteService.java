package com.techuniversity.clientes;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ClienteService {
    static MongoCollection<Document> clientes;

    private static MongoCollection<Document> getClientesCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongoClient = MongoClients.create(settings);
        MongoDatabase database = mongoClient.getDatabase("dbcli");
        return database.getCollection("clientes");
    }


    public static void insert(String strServicios) throws Exception{
        clientes = getClientesCollection();
        Document doc = Document.parse(strServicios);
        List<Document> lst = doc.getList("clientes", Document.class);
        if (lst == null){
            clientes.insertOne(doc);
        }else {
            clientes.insertMany(lst);
        }
    }

    public static List getAll() {
        clientes = getClientesCollection();
        List list = new ArrayList();
        FindIterable<Document> findIterable = clientes.find();
        Iterator it = findIterable.iterator();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }

    public static List getFiltrados(String filtro){
        clientes = getClientesCollection();
        List list = new ArrayList();
        Document docFiltro = Document.parse(filtro);
        FindIterable<Document> iterDoc = clientes.find(docFiltro);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }

    public static void update(String filtro, String updates) {
        clientes = getClientesCollection();
        Document docFiltro = Document.parse(filtro);
        Document doc = Document.parse(updates);
        clientes.updateOne(docFiltro, doc);
    }

}
