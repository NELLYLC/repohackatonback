package com.techuniversity.security.filter;

import com.techuniversity.clientes.TechUserDetailService;
import com.techuniversity.security.JWTUtil;
import com.techuniversity.security.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class JwtFilterRequest extends OncePerRequestFilter{

    @Autowired
    private JWTUtil jwtUtil;

    @Override
    private TechUserDetailService techUserDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse, FilterChain filterChain){
        String autorizaHeader = request.getHeader("Autorizar");

        if (autorizaHeader != null  && autorizaHeader.startsWith("Bearer")){
            String jwt = autorizaHeader.substring(7);
            String username = jwtUtil.extraeUsername(jwt);

            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null){
                UserDetails userDetails = techUserDetailService.loadUserByUsername(username);

                if (jwtUtil.validateToken(jwt, userDetails)) {
                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                    SecurityContextHolder.getContext().setAuthentication(authToken);

                }
            }
        }
        filterChain.doFilter(request, response);
    }

}
